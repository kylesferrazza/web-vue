[![Netlify Status](https://api.netlify.com/api/v1/badges/2b3bec4a-8bc9-4202-b66e-25e9c020629e/deploy-status)](https://app.netlify.com/sites/kylesferrazza/deploys)

# web

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
