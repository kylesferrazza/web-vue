{ pkgs ? import <nixpkgs> {} }:
pkgs.mkShell {
  name = "web";
  buildInputs = with pkgs; [
    nodePackages."@vue/cli"
    nodePackages.yarn
    nodejs
  ];
}
