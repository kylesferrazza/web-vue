import { createRouter, createWebHistory } from "vue-router";
import Home from "@/components/Home";
// import About from "@/components/About";
import Contact from "@/components/Contact";
import PageNotFound from "@/components/PageNotFound";

export const routes = [
  {
    path: "/",
    name: "Home",
    component: Home,
  },
  {
    path: "/contact",
    name: "Contact",
    component: Contact,
  },
  // {
  //   path: "/about",
  //   name: "About",
  //   component: About,
  // },
];

const hiddenRoutes = [
  {
    path: "/:pathMatch(.*)*",
    name: "PageNotFound",
    component: PageNotFound,
  }
];

const allRoutes = [...routes, ...hiddenRoutes];

const router = createRouter({
  history: createWebHistory(),
  routes: allRoutes,
})

export default router;
